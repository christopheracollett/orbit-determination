# Easy Orbits
Calculate orbital characteristics for any satellite around any celestial body given Keplerian orbital elements. Plots for a single orbital period can be created and saved with the `save_orbit_plot` function.

```python
>>> from orbit_determination import CelestialBody, Satellite, Orbit, save_orbit_plot
>>>
>>> earth = CelestialBody(name='Earth', radius=6378.137, mass=5.97219e24)
>>> sat = Satellite(body=earth, semimajor_axis=3e4, eccentricity=0.3, inclination=0.9, argument_of_periapsis=1.2, longitude_of_ascending_node=0.3)
>>> orbit = Orbit(satellite=sat, starting_true_anomaly=3.0)
>>> print(sat.altitude(true_anomaly=0))
14621.863000000001 kilometer
>>> print(orbit.inertial_position)
[[-11970.68386083 -25725.77407879 -26512.71033663]
 [-11909.37121843 -25730.69818773 -26541.47131285]
 [-11847.99422736 -25735.51180267 -26570.1232317 ]
 ...
 [-12093.11448736 -25715.59465465 -26454.86214436]
 [-12031.93175127 -25720.7395445  -26483.84053584]
 [-11970.68386083 -25725.77407879 -26512.71033663]]
>>> save_orbit_plot(orbit, 'orbit_of_satellite.pdf')
```

All available parameters can be found by using the Python `help` function (i.e., `help(Satellite)` or `help(Orbit)`).

Additionally, the orbit_determination package can be used from the command line to create a CSV of the propagated orbit and a PDF plot of the 2D orbit in the orbital reference plane. Use `$ python -m orbit_determination -h` to obtain additional details.

```console
$ python -m orbit_determination -body_name Earth -body_radius 6378.137 -body_mass 5.97219e24 -semimajor_axis 42164.137 -eccentricity 0.5 -inclination 5 -argument_of_periapsis 15 -longitude_of_ascending_node 30 -true_anomaly 15 -csv_file_path sat_orbiting_earth.csv -figure_file_path sat_orbiting_earth.pdf
Saved orbit propagation at sat_orbiting_earth.csv.
Saved plot at sat_orbiting_earth.pdf.
```

# Installation
To install, simply download the latest wheel (`.whl`) file from the [Releases](https://gitlab.com/christopheracollett/orbit-determination/-/releases) page and install using pip. All required dependencies will be installed automatically.

```console
$ pip install orbit_determination-1.1.0-py3-none-any.whl
...
Successfully installed ... orbit-determination-1.1.0 ...
$
```

# Dependencies
Python version: 3.10 or greater

Package dependencies:
- pint==0.18
- numpy==1.22.3
- matplotlib==3.5.1

Package dependencies for development (in addition to the above):
- pytest==7.0.0
- setuptools==58.1.0
- build==0.7.0


# Design Considerations
Test Driven Development (TDD) was used to develop all core code - only plotting and the command line interface (CLI) are not tested due to the low cost-benefit of such testing. If not using Visual Studio Code to make use of the `.env` file (see [Use of the PYTHONPATH variable - Visual Studio Code](https://code.visualstudio.com/docs/python/environments#_use-of-the-pythonpath-variable)), you can run the tests by changing the current working directory to the `app` folder and running `python -m pytest ..` to discover all tests in the `tests` folder.

An emphasis was placed on increasing the readability and usability of all executed code to reduce the need for comments, to the point that any comments are unnecessary (provided the user has adequate domain knowledge of orbital mechanics).

Only a 2D plot of the orbit in the orbital reference plane is created. Although 3D plotting is possible, the standard plotting package for Python (`matplotlib`) does not have adequate means of creating a high-quality 3D orbital plot without significant effort, placing 3D plots beyond the scope of this package.

No effort was made to prevent the use of impossible orbits (such as an orbit that is smaller than the celestial body). Using non-elliptical orbits (parabolic or hyperbolic) or eccentricities that are less than 0 will produce a `ValueError`, however.

Plots are saveable using the `save_orbit_plot` function. The plots are not opened in the `matplotlib` native GUI to support Linux users that may not have their system configured to allow `matplotlib` to connect to an X display or a Wayland display (see [Selecting a Backend - Matplotlib](https://matplotlib.org/stable/users/explain/backends.html#selecting-a-backend)).

All primary classes and functions are available at the top-level of the package - i.e., you can perform `from orbit_determination import ...` to import everything you need to use the package in your project.

The `pint` package is used to define all units to improve the readability of the code, its API, and its output, as well as to catch any invalid operations (such as adding kilometers to kilograms). See the [Pint package on PyPI](https://pypi.org/project/Pint/) for more details.
