from dataclasses import dataclass

from .units import kg, km, s, is_unit


@dataclass
class CelestialBody:
    name: str
    radius: float = 0*km
    mass: float = 0*kg

    gravitational_constant = 6.67430e-20 * km**3/kg/s**2

    def __post_init__(self):
        self._set_units()

    @property
    def standard_gravitational_parameter(self):
        return self.gravitational_constant*self.mass

    def _set_units(self):
        if not is_unit(self.radius, km):
            self.radius *= km
        if not is_unit(self.mass, kg):
            self.mass *= kg
