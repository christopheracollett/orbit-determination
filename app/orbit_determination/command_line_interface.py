import argparse

from .body import CelestialBody
from .csv import save_orbit_propagation
from .orbit import Orbit
from .plot import save_orbit_plot
from .satellite import Satellite
from .units import degrees_to_radians


def parse_arguments():
    parser = argparse.ArgumentParser(
        description=('Create a csv of the propagated orbit of a spacecraft '
                     'given Keplerian orbital elements. '
                     'Also creates a 2D plot the orbit in the orbital plane.'))
    parser.add_argument(
        '-body_name',
        required=True,
        help='The name of the celestial body.')
    parser.add_argument(
        '-body_radius',
        required=True,
        type=float,
        help='The radius of the celestial body (km).')
    parser.add_argument(
        '-body_mass',
        required=True,
        type=float,
        help='The mass of the celestial body (kg).')
    parser.add_argument(
        '-semimajor_axis',
        required=True,
        type=float,
        help='The semimajor axis of the satellite (km).')
    parser.add_argument(
        '-eccentricity',
        required=True,
        type=float,
        help='The eccentricity (e) of the satellite (0 <= e < 1).')
    parser.add_argument(
        '-inclination',
        required=True,
        type=float,
        help='The inclination of the satellite (deg).')
    parser.add_argument(
        '-argument_of_periapsis',
        required=True,
        type=float,
        help='The argument of periapsis of the satellite (deg).')
    parser.add_argument(
        '-longitude_of_ascending_node',
        required=True,
        type=float,
        help='The longitude of ascending node of the satellite (deg).')
    parser.add_argument(
        '-true_anomaly',
        required=True,
        type=float,
        help=('The true anomaly of the satellite at the initial time (deg).'))
    parser.add_argument(
        '-csv_file_path',
        required=True,
        type=str,
        help=('The file path (relative or absolute) '
              'to save the orbital propagation (e.g., orbit.csv).'))
    parser.add_argument(
        '-figure_file_path',
        required=True,
        type=str,
        help=('The file path (relative or absolute) '
              'to save the orbital plot (e.g., figure.pdf).'))
    return parser.parse_args()


def cli():
    args = parse_arguments()
    body = CelestialBody(args.body_name, args.body_radius, args.body_mass)
    true_anomaly = degrees_to_radians(args.true_anomaly)
    inclination = degrees_to_radians(args.inclination)
    argument_of_periapsis = degrees_to_radians(args.argument_of_periapsis)
    longitude_of_ascending_node = degrees_to_radians(
        args.longitude_of_ascending_node)
    satellite = Satellite(
        body,
        args.semimajor_axis,
        args.eccentricity,
        inclination,
        argument_of_periapsis,
        longitude_of_ascending_node)
    orbit = Orbit(satellite, starting_true_anomaly=true_anomaly)
    save_orbit_propagation(orbit, args.csv_file_path)
    save_orbit_plot(orbit, args.figure_file_path)
    return (f'Saved orbit propagation at {args.csv_file_path}.\n'
            f'Saved plot at {args.figure_file_path}.')
