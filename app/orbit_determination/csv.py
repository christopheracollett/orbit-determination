import numpy as np
from numpy.typing import NDArray

from .orbit import Orbit


def save_orbit_propagation(orbit: Orbit, csv_file_path: str):
    csv = build_csv(orbit.full_propagation)
    with open(csv_file_path, 'w') as file:
        file.write(csv)


def build_csv(columns_with_headers: dict[str, list]):
    columns = np.array([
        _add_header_to_values(header, column)
        for header, column in columns_with_headers.items()])
    return _rows_to_csv(_columns_to_rows(columns))


def _add_header_to_values(header: str, values: list):
    return [header, *values]


def _columns_to_rows(columns: NDArray):
    return columns.transpose()


def _rows_to_csv(rows: list[list[str]]):
    return '\n'.join([','.join(row) for row in rows])
