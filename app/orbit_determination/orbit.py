from dataclasses import dataclass
from typing import Callable
import numpy as np
from numpy.typing import NDArray
from pint import Quantity

from .satellite import Satellite
from .units import rad, radians_to_degrees


@dataclass
class Orbit:
    satellite: Satellite
    starting_true_anomaly: float = 0*rad
    number_points: float = 3601

    _true_anomalies = None

    @property
    def true_anomalies(self) -> NDArray:
        if self._true_anomalies is None:
            self._true_anomalies = np.linspace(
                start=self.starting_true_anomaly,
                stop=self.starting_true_anomaly + 2*np.pi,
                num=self.number_points)
        return self._true_anomalies

    @property
    def radii(self):
        return self._get_all(self.satellite.radius)

    @property
    def altitudes(self):
        return self._get_all(self.satellite.altitude)

    @property
    def velocities(self):
        return self._get_all(self.satellite.velocity)

    @property
    def times(self):
        return self._get_all(
            self.satellite.time_from_start, self.starting_true_anomaly)

    @property
    def inertial_position(self):
        return self._get_all(self.satellite.inertial_position)

    @property
    def inertial_velocity(self):
        return self._get_all(self.satellite.inertial_velocity)

    @property
    def full_propagation(self) -> dict[str, NDArray]:
        return {
            'True Anomaly (deg)': radians_to_degrees(self.true_anomalies),
            'Radius (km)': self.radii,
            'Altitude (km)': self.altitudes,
            'Velocity (km/s)': self.velocities,
            'Time (s)': self.times,
            'Inertial Position X (km)': self.inertial_position[:, 0],
            'Inertial Position Y (km)': self.inertial_position[:, 1],
            'Inertial Position Z (km)': self.inertial_position[:, 2],
            'Inertial Velocity X (km/s)': self.inertial_velocity[:, 0],
            'Inertial Velocity Y (km/s)': self.inertial_velocity[:, 1],
            'Inertial Velocity Z (km/s)': self.inertial_velocity[:, 2]}

    def _get_all(self, characteristic: Callable, *additional_args):
        values = [
            characteristic(true_anomaly, *additional_args)
            for true_anomaly in self.true_anomalies]
        if isinstance(values[0], Quantity):
            return np.array([value.magnitude for value in values])
        return np.array(values)
