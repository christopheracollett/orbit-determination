from matplotlib import pyplot
import numpy as np

from .orbit import Orbit


def save_orbit_plot(orbit: Orbit, figure_file_path: str):
    figure, axis = pyplot.subplots(subplot_kw={'projection': 'polar'})
    _plot_orbit(axis, orbit)
    _plot_body(axis, orbit)
    _clear_radial_labels(axis)
    _set_max_radius(axis, orbit)
    axis.grid(True)
    axis.set_title(f'Plot of a satellite around {orbit.satellite.body.name}.')
    pyplot.legend(loc='best')
    pyplot.savefig(figure_file_path)


def _plot_orbit(axis: pyplot.Axes, orbit: Orbit):
    axis.plot(orbit.true_anomalies, orbit.radii, color='blue')
    periapsis = orbit.satellite.periapsis.magnitude
    axis.plot([0], [periapsis], color='red', marker='^',
              label=f'Periapsis ({periapsis:.3e} km)')
    apoapsis = orbit.satellite.apoapsis.magnitude
    axis.plot([np.pi], [apoapsis], color='red', marker='v',
              label=f'Apoapsis ({apoapsis:.3e} km)')
    start_radius = orbit.radii[0]
    axis.plot([orbit.starting_true_anomaly], [start_radius], color='black',
              marker='o', label='Start position')


def _plot_body(axis: pyplot.Axes, orbit: Orbit):
    origin = (0, 0)
    radius = orbit.satellite.body.radius.magnitude
    polar_plot_transformation = axis.transProjectionAffine + axis.transAxes
    body_circle = pyplot.Circle(
        origin, radius, transform=polar_plot_transformation, color='black')
    axis.add_artist(body_circle)


def _clear_radial_labels(axis: pyplot.Axes):
    axis.set_rticks([])


def _set_max_radius(axis: pyplot.Axes, orbit: Orbit):
    axis.set_ylim(0, orbit.satellite.apoapsis.magnitude * 1.2)
