from dataclasses import dataclass
import numpy as np
from numpy.typing import NDArray

from .body import CelestialBody
from .units import km, rad, is_unit


@dataclass
class Satellite:
    body: CelestialBody
    semimajor_axis: float = 0*km
    eccentricity: float = 0
    inclination: float = 0*rad
    argument_of_periapsis: float = 0*rad
    longitude_of_ascending_node: float = 0*rad

    def __post_init__(self):
        self._check_eccentricity()
        self._set_units()

    @property
    def periapsis(self):
        return self.semimajor_axis * (1-self.eccentricity)

    @property
    def apoapsis(self):
        return self.semimajor_axis * (1+self.eccentricity)

    @property
    def major_axis(self):
        return self.semimajor_axis*2

    @property
    def minor_axis(self):
        return self.semimajor_axis * (1-self.eccentricity**2)**0.5

    @property
    def semilatus_rectum(self):
        return self.semimajor_axis * (1-self.eccentricity**2)

    @property
    def latus_rectum(self):
        return self.semilatus_rectum*2

    @property
    def period(self):
        radicand = (self.semimajor_axis**3
                    / self.body.standard_gravitational_parameter)
        return 2*np.pi*radicand**0.5

    @property
    def specific_angular_momentum(self):
        radicand = (self.semilatus_rectum
                    * self.body.standard_gravitational_parameter)
        return radicand**0.5

    def radius(self, true_anomaly=0*rad):
        return self.semilatus_rectum/(1+self.eccentricity*np.cos(true_anomaly))

    def altitude(self, true_anomaly=0*rad):
        return self.radius(true_anomaly) - self.body.radius

    def velocity(self, true_anomaly=0*rad):
        radicand = (self.body.standard_gravitational_parameter
                    * (2/self.radius(true_anomaly) - 1/self.semimajor_axis))
        return radicand**0.5

    def inertial_position(self, true_anomaly=0*rad):
        unit_vector = np.array([
            np.cos(true_anomaly),
            np.sin(true_anomaly),
            0])
        orbital_position_vector = (self.radius(true_anomaly).magnitude
                                   * unit_vector)
        return self._transform(orbital_position_vector)

    def inertial_velocity(self, true_anomaly=0*rad):
        radical = ((self.body.standard_gravitational_parameter
                    / self.semilatus_rectum)**0.5)
        orbital_velocity_vector = np.array([
            -radical.magnitude*np.sin(true_anomaly),
            radical.magnitude*(self.eccentricity + np.cos(true_anomaly)),
            0
        ])
        return self._transform(orbital_velocity_vector)

    def eccentric_anomaly(self, true_anomaly=0*rad):
        if self.eccentricity == 0:
            return true_anomaly
        numerator = self.semimajor_axis - self.radius(true_anomaly)
        denominator = self.semimajor_axis * self.eccentricity
        return self._correct_quadrant(
            eccentric_anomaly=np.arccos(numerator/denominator),
            true_anomaly=true_anomaly)

    def time_at_periapsis(self, starting_true_anomaly=0*rad):
        return -self._time_from_periapsis(starting_true_anomaly)

    def time_from_start(self, true_anomaly=0*rad, starting_true_anomaly=0*rad):
        return (self._time_from_periapsis(true_anomaly)
                + self.time_at_periapsis(starting_true_anomaly))

    def _transform(self, orbital_vector: NDArray):
        return np.array([(orbital_vector*row).sum()
                         for row in self._euler_matrix()])

    def _euler_matrix(self):
        return euler_angle_transformation_matrix(
            self.inclination,
            self.argument_of_periapsis,
            self.longitude_of_ascending_node)

    def _time_from_periapsis(self, true_anomaly):
        eccentric_anomaly = self.eccentric_anomaly(true_anomaly)
        numerator = (eccentric_anomaly
                     - self.eccentricity*np.sin(eccentric_anomaly))
        denominator = (self.body.standard_gravitational_parameter
                       / self.semimajor_axis**3)
        return numerator/denominator**0.5

    def _correct_quadrant(self, eccentric_anomaly: float, true_anomaly: float):
        if self._in_return_orbit(true_anomaly):
            eccentric_anomaly += 2*(np.pi - eccentric_anomaly)
        num_extra_orbits = true_anomaly // (2*np.pi)
        return eccentric_anomaly + num_extra_orbits * 2*np.pi

    def _in_return_orbit(self, true_anomaly: float):
        return _is_odd(true_anomaly//np.pi)

    def _check_eccentricity(self):
        if self.eccentricity < 0 or self.eccentricity >= 1:
            raise ValueError('Expected eccentricity (e) to be 0 <= e < 1.')

    def _set_units(self):
        if not is_unit(self.semimajor_axis, km):
            self.semimajor_axis *= km


def euler_angle_transformation_matrix(
        inclination, argument_of_periapsis, longitude_of_ascending_node):
    c_p, s_p = _cos_and_sin(argument_of_periapsis)
    c_a, s_a = _cos_and_sin(longitude_of_ascending_node)
    c_i, s_i = _cos_and_sin(inclination)
    return np.array([
        [c_p*c_a - s_p*c_i*s_a, -s_p*c_a - c_p*c_i*s_a, s_a*s_i],
        [c_p*s_a + s_p*c_i*c_a, - s_p*s_a + c_p*c_i*c_a, -c_a*s_i],
        [s_p*s_i, c_p*s_i, c_i]
    ])


def _cos_and_sin(value=0*rad):
    return np.cos(value), np.sin(value)


def _is_odd(value: float):
    return value % 2 != 0
