from pint import UnitRegistry, Quantity, Unit
import numpy as np


_REGISTRY = UnitRegistry()


km = _REGISTRY.kilometer
kg = _REGISTRY.kilogram
s = _REGISTRY.second
rad = _REGISTRY.radian


def is_unit(value, unit: Unit):
    return isinstance(value, Quantity) and value.units == unit


_DEG_RAD_CONVERSION = np.pi/180


def degrees_to_radians(angle: float):
    return angle * _DEG_RAD_CONVERSION


def radians_to_degrees(angle: float):
    return angle * 1/_DEG_RAD_CONVERSION
