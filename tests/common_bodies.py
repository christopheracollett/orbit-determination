import numpy as np

from orbit_determination import CelestialBody, Satellite


def geostationary_satellite():
    earth = Earth()
    return Satellite(
        body=earth,
        semimajor_axis=35786+earth.radius.magnitude,
        eccentricity=0.0)


def generic_satellite():
    return Satellite(
        body=Earth(),
        semimajor_axis=1e4,
        eccentricity=0.2)


def satellite_in_3d():
    earth = Earth()
    return Satellite(
        body=earth,
        semimajor_axis=3*earth.radius.magnitude,
        eccentricity=0.2,
        inclination=np.pi/4,
        argument_of_periapsis=-np.pi/4,
        longitude_of_ascending_node=5*np.pi/4)


def Earth():
    return CelestialBody('Earth', radius=6378.137, mass=5.97219e24)
