import numpy as np

from orbit_determination.csv import build_csv


def test_build_csv():
    columns_with_headers = {
        'header1': np.array([1, 2, 3]),
        'header2': np.array([4, 5, 6])}
    assert build_csv(columns_with_headers) == 'header1,header2\n1,4\n2,5\n3,6'
