import numpy as np

from orbit_determination import Orbit
from orbit_determination.units import radians_to_degrees

from .common_bodies import generic_satellite, geostationary_satellite


def test_orbit_true_anomalies():
    sat = geostationary_satellite()
    start = np.pi/2
    num = 100
    orbit = Orbit(sat, starting_true_anomaly=start, number_points=num)
    true_anomalies = _get_true_anomalies(start=start, num=num)
    assert (orbit.true_anomalies == true_anomalies).all()


def test_orbit_radii_and_altitudes():
    sat = geostationary_satellite()
    start = np.pi/2
    num = 100
    orbit = Orbit(sat, starting_true_anomaly=start, number_points=num)
    radii = np.array([sat.radius(0).magnitude]*num)
    altitudes = np.array([sat.altitude(0).magnitude]*num)
    assert (orbit.radii == radii).all()
    assert (orbit.altitudes == altitudes).all()


def test_orbit_inertial_position():
    sat = geostationary_satellite()
    orbit = Orbit(sat, number_points=100)
    assert (orbit.inertial_position[0] == sat.inertial_position(0)).all()


def test_orbit_inertial_velocity():
    sat = geostationary_satellite()
    orbit = Orbit(sat, number_points=100)
    assert (orbit.inertial_velocity[0] == sat.inertial_velocity(0)).all()


def test_orbit_velocities():
    sat = geostationary_satellite()
    num = 100
    orbit = Orbit(sat, number_points=num)
    velocities = np.array([3.074666824970851]*num)
    assert (orbit.velocities == velocities).all()


def test_orbit_times():
    sat = generic_satellite()
    orbit = Orbit(sat, starting_true_anomaly=np.pi/2, number_points=100)
    times = orbit.times
    assert times[-1] - times[0] == sat.period.magnitude


def test_orbit_makes_full_orbit():
    sat = generic_satellite()
    orbit = Orbit(sat, starting_true_anomaly=np.pi/4, number_points=100)
    assert orbit.radii[0] == orbit.radii[-1]


def test_orbit_full_propagation():
    sat = generic_satellite()
    orbit = Orbit(sat, starting_true_anomaly=np.pi/2, number_points=100)
    propagation = orbit.full_propagation
    true_anomalies = radians_to_degrees(orbit.true_anomalies)
    assert (propagation['True Anomaly (deg)'] == true_anomalies).all()
    assert (propagation['Radius (km)'] == orbit.radii).all()
    assert (propagation['Altitude (km)'] == orbit.altitudes).all()
    assert (propagation['Velocity (km/s)'] == orbit.velocities).all()
    assert (propagation['Time (s)'] == orbit.times).all()


def test_orbit_full_propagation_inertial_position():
    sat = generic_satellite()
    orbit = Orbit(sat, starting_true_anomaly=np.pi/2, number_points=100)
    propagation = orbit.full_propagation
    positions = np.array([
        propagation['Inertial Position X (km)'],
        propagation['Inertial Position Y (km)'],
        propagation['Inertial Position Z (km)']])
    assert (positions.transpose() == orbit.inertial_position).all()


def test_orbit_full_propagation_inertial_velocity():
    sat = generic_satellite()
    orbit = Orbit(sat, starting_true_anomaly=np.pi/2, number_points=100)
    propagation = orbit.full_propagation
    velocities = np.array([
        propagation['Inertial Velocity X (km/s)'],
        propagation['Inertial Velocity Y (km/s)'],
        propagation['Inertial Velocity Z (km/s)']])
    assert (velocities.transpose() == orbit.inertial_velocity).all()


def _get_true_anomalies(start, num):
    return np.linspace(start=start, stop=start+2*np.pi, num=num)
