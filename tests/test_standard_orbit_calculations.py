import numpy as np
import pytest

from orbit_determination import Satellite
from orbit_determination import CelestialBody
from orbit_determination.satellite import euler_angle_transformation_matrix
from orbit_determination.units import km, kg, s

from .common_bodies import (
    Earth,
    geostationary_satellite,
    generic_satellite,
    satellite_in_3d)


def test_body_components():
    earth = Earth()
    assert earth.radius == 6378.137 * km
    assert earth.mass == 5.97219e24 * kg
    assert earth.gravitational_constant == (
        6.67430e-20 * km**3/kg/s**2)
    assert earth.standard_gravitational_parameter == (
        6.67430e-20*5.97219e24 * km**3/s**2)


def test_satellite_units():
    sat = geostationary_satellite()
    assert sat.semimajor_axis == 42164.137 * km
    assert sat.eccentricity == 0.0


def test_orbital_shape_characteristics():
    sat = generic_satellite()
    assert sat.periapsis == 1e4*0.8 * km
    assert sat.apoapsis == 1.e4*1.2 * km
    assert sat.major_axis == 1.e4*2 * km
    assert sat.minor_axis == 1.e4*(1-0.2**2)**0.5 * km
    assert sat.semilatus_rectum == 1.e4*(1-0.2**2) * km
    assert sat.latus_rectum == 1.e4*(1-0.2**2)*2 * km


def test_characteristics_based_on_body():
    earth = Earth()
    sat = geostationary_satellite()
    assert round(sat.period) == (23*3600 + 56*60 + 4) * s
    assert sat.specific_angular_momentum == (
        sat.semilatus_rectum*earth.standard_gravitational_parameter)**0.5


def test_radius():
    sat = generic_satellite()
    assert sat.radius(true_anomaly=0) == sat.periapsis
    assert sat.radius(true_anomaly=np.pi/2) == sat.semilatus_rectum
    assert sat.radius(true_anomaly=np.pi) == sat.apoapsis


def test_altitude():
    sat = geostationary_satellite()
    assert sat.altitude(true_anomaly=0) == 35786 * km


def test_velocity():
    sat = generic_satellite()
    assert sat.velocity(true_anomaly=0) == 7.732417576379331 * km/s
    assert geostationary_satellite().velocity(0) == (
        3.074666824970851 * km/s)


def test_eccentric_anomaly_for_circular_orbits():
    sat = geostationary_satellite()
    true_anomalies = [np.pi/2, np.pi, 3*np.pi/2, 2*np.pi, 5*np.pi/2]
    for true_anomaly in true_anomalies:
        assert sat.eccentric_anomaly(true_anomaly) == true_anomaly


def test_eccentric_anomaly_for_non_circular_orbits():
    sat = generic_satellite()
    true_anomalies = [0, np.pi, 2*np.pi, 3*np.pi, 4*np.pi]
    for true_anomaly in true_anomalies:
        assert sat.eccentric_anomaly(true_anomaly) == true_anomaly


def test_time_at_periapsis():
    sat = generic_satellite()
    assert sat.time_at_periapsis(starting_true_anomaly=0) == 0
    assert sat.time_at_periapsis(starting_true_anomaly=np.pi) == -sat.period/2


def test_time_from_start():
    sat = generic_satellite()
    assert sat.time_from_start(true_anomaly=0, starting_true_anomaly=0) == 0
    assert sat.time_from_start(np.pi, 0) == sat.period/2
    assert sat.time_from_start(2*np.pi, 0) == sat.period
    assert sat.time_from_start(np.pi, np.pi) == 0
    assert sat.time_from_start(2*np.pi, np.pi) == sat.period/2


def test_position_vector():
    earth = Earth()
    sat = satellite_in_3d()
    expected_vector = np.array([
        1.6772*earth.radius.magnitude,
        -1.6772*earth.radius.magnitude,
        2.3719*earth.radius.magnitude])
    assert (sat.inertial_position(3*np.pi/4).round(1)
            == expected_vector.round(1)).all()


def test_position_vector_in_2d():
    sat = geostationary_satellite()
    radius = sat.radius(0).magnitude
    assert (sat.inertial_position(0).round(10)
            == np.array([radius, 0, 0]).round(10)).all()
    assert (sat.inertial_position(np.pi/2).round(10)
            == np.array([0, radius, 0]).round(10)).all()
    assert (sat.inertial_position(np.pi).round(10)
            == np.array([-radius, 0, 0]).round(10)).all()
    assert (sat.inertial_position(3*np.pi/2).round(10)
            == np.array([0, -radius, 0]).round(10)).all()


def test_velocity_vector():
    sat = satellite_in_3d()
    expected_vector = np.array([3.157, 2.499, 0.466])
    assert (sat.inertial_velocity(3*np.pi/4).round(3)
            == expected_vector.round(3)).all()


def test_velocity_vector_in_2d():
    sat = geostationary_satellite()
    velocity = sat.velocity(0).magnitude
    assert (sat.inertial_velocity(0).round(10)
            == np.array([0, velocity, 0]).round(10)).all()
    assert (sat.inertial_velocity(np.pi/2).round(10)
            == np.array([-velocity, 0, 0]).round(10)).all()
    assert (sat.inertial_velocity(np.pi).round(10)
            == np.array([0, -velocity, 0]).round(10)).all()
    assert (sat.inertial_velocity(3*np.pi/2).round(10)
            == np.array([velocity, 0, 0]).round(10)).all()


def test_eccentricity_between_0_and_1():
    with pytest.raises(ValueError):
        Satellite(
            body=Earth(),
            semimajor_axis=1e4,
            eccentricity=-0.5)
    with pytest.raises(ValueError):
        Satellite(
            body=Earth(),
            semimajor_axis=1e4,
            eccentricity=1.0)


def test_initialize_with_units():
    body = CelestialBody(
        name='object',
        radius=1e4*km,
        mass=1e8*kg)
    assert body.radius.units == km
    assert body.mass.units == kg
    sat = Satellite(
        body=Earth(),
        semimajor_axis=1e4*km,
        eccentricity=0)
    assert sat.semimajor_axis.units == km


def test_euler_angle_transformation_matrix():
    matrix = euler_angle_transformation_matrix(
        inclination=np.pi/4,
        argument_of_periapsis=np.pi/2,
        longitude_of_ascending_node=5*np.pi/4)
    expected_matrix = np.array([
        [0.5, 0.7071, -0.5],
        [-0.5, 0.7071, 0.5],
        [0.7071, 0, 0.7071]
    ])
    assert (matrix.round(4) == expected_matrix.round(4)).all()
