import numpy as np

from orbit_determination.units import degrees_to_radians, radians_to_degrees


def test_conversions():
    assert degrees_to_radians(180) == np.pi
    assert radians_to_degrees(3*np.pi/4) == 135
